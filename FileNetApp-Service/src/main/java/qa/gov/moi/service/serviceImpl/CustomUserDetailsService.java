package qa.gov.moi.service.serviceImpl;


import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import qa.gov.moi.dao.repo.UserRepository;
import qa.gov.moi.model.entity.user.User;
import qa.gov.moi.util.mapper.OrikaBeanMapper;
/**
 * 
 * @ author Shahnawaz Alam
 *
 */

@Service
public class CustomUserDetailsService implements UserDetailsService {
	
    @Autowired
    private UserRepository repository;
    
    @Autowired
    private OrikaBeanMapper mapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    	
    	User user = repository.findByUserName(username);
        return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(), new ArrayList<>());
    }
}
