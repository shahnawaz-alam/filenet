package qa.gov.moi.service.dto;



import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import qa.gov.moi.service.util.ServiceConstants;
/**
 * 
 * @ author Shahnawaz Alam
 *
 */
public class AuthRequestDto implements Serializable {
	

	private static final long serialVersionUID = 1L;
	
	@NotEmpty(message = ServiceConstants.USER_NAME)
	@NotBlank(message = ServiceConstants.USER_NAME)
	@NotNull(message = ServiceConstants.USER_NAME)
    private String userName;
	@NotEmpty(message = ServiceConstants.USER_PASSWORD)
	@NotBlank(message = ServiceConstants.USER_PASSWORD)
	@NotNull(message = ServiceConstants.USER_PASSWORD)
    private String password;
    
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
    
}
