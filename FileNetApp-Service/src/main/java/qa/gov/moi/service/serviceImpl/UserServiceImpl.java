package qa.gov.moi.service.serviceImpl;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

import qa.gov.moi.service.UserService;
import qa.gov.moi.service.dto.AuthRequestDto;
import qa.gov.moi.util.exception.BusinessException;
import qa.gov.moi.util.logger.MOILogger;
import qa.gov.moi.util.loggerImpl.MOILogFactory;
import qa.gov.moi.util.resolver.CodeEnum;
import qa.gov.moi.util.security.JwtTokenUtil;
/**
 * 
 * @ author Shahnawaz Alam
 *
 */

@Service
public class UserServiceImpl implements UserService {

	private static final MOILogger logger = MOILogFactory.getLoggerInstance(UserServiceImpl.class.getName());

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private JwtTokenUtil jwtUtil;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Override
	public String authenticateUser(AuthRequestDto authRequest) throws BusinessException {

		if (null != authRequest && !authRequest.getUserName().isEmpty()) {
			authRequest.setUserName(authRequest.getUserName().trim());
		}
		if (null != authRequest && !authRequest.getPassword().isEmpty()) {
			authRequest.setPassword(authRequest.getPassword().trim());
		}

		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUserName(), authRequest.getPassword()));
		} catch (Exception e) {
			logger.info("---- Invalid username/password ----");
			throw new BusinessException("UNSUCCESSFUL_AUTHENTICATE",messageSource.getMessage(CodeEnum.UNSUCCESSFUL_AUTHENTICATE.getValue(), null, Locale.ENGLISH));
		}
		return jwtUtil.generateToken(authRequest.getUserName());

	}

}
