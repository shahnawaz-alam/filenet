/**
 * 
 */
package qa.gov.moi.service;

import qa.gov.moi.service.dto.AuthRequestDto;
import qa.gov.moi.util.exception.BusinessException;

/**
 * @author Shahnawaz Alam
 *
 */
public interface UserService {

	public String authenticateUser(AuthRequestDto authRequest) throws BusinessException;

}
