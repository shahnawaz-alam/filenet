package qa.gov.moi.util.loggerImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import qa.gov.moi.util.logger.MOILogger;

/**
 * 
 * @ author Shahnawaz Alam
 *
 */

public class MOISlf4jLoggerImpl implements MOILogger {

	private transient final Logger logger;

	public MOISlf4jLoggerImpl(String className) {
		logger = LoggerFactory.getLogger(className);
	}

	public void trace(String message) {
		message = logger.getName() + ": " + message;
		logger.trace(message);
	}

	public void info(String message) {
		message = logger.getName() + ": " + message;
		logger.info(message);
	}

	public void debug(String message) {
		message = logger.getName() + ": " + message;
		logger.debug(message);
	}

	public void warn(String message) {
		message = logger.getName() + ": " + message;
		logger.warn(message);
	}

	public void error(String message) {
		message = logger.getName() + ": " + message;
		logger.error(message);
	}

	public void trace(String message, final Throwable thrower) {
		message = logger.getName() + ": " + message;
		logger.trace(message, thrower);
	}

	public void info(String message, final Throwable thrower) {
		message = logger.getName() + ": " + message;
		logger.info(message, thrower);
	}

	public void debug(String message, final Throwable thrower) {
		message = logger.getName() + ": " + message;
		logger.debug(message, thrower);
	}

	public void warn(String message, final Throwable thrower) {
		message = logger.getName() + ": " + message;
		logger.warn(message, thrower);
	}

	public void error(String message, final Throwable thrower) {
		message = logger.getName() + ": " + message;
		logger.error(message, thrower);
	}

}