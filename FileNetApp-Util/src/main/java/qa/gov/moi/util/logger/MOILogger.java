package qa.gov.moi.util.logger;


/**
 * @author Shahnawaz Alam
 *
 */

public interface MOILogger {

	public void trace(String message);
	public void warn(String message);
	public void info(String message);
	public void debug(String message);
	public void error(String message);
	

	public void trace(String message, Throwable thrower);
	public void warn(String message, Throwable thrower);
	public void info(String message, Throwable thrower);
	public void debug(String message, Throwable thrower);
	public void error(String message, Throwable thrower);

}