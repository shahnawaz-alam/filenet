package qa.gov.moi.util.loggerImpl;


import java.util.logging.Logger;

import qa.gov.moi.util.logger.MOILogger;

/**
 * 
 * @ author Shahnawaz Alam
 *
 */

public class MOILogFactory {

	public static MOILogger getLoggerInstance(final String className) {
		MOILogger logger = new MOILog4j2LoggerImpl(className);
		return logger;
	}

	public static MOILogger getLoggerInstance(final String className, final String contractName) {
		MOILogger logger = new MOILog4j2LoggerImpl(className);
		return logger;
	}
	
	public static void printLogs() {
		printSlf4jLogs();
		printLog4j2Logs();
		printJulLogs();
	}
	
	public static void printSlf4jLogs() {
		final MOILogger logger = new MOISlf4jLoggerImpl(MOILogFactory.class.getName());
		logger.trace("--------- Slf4j TRACE  Log ---------");
		logger.error("--------- Slf4j ERROR  Log ---------");
		logger.warn("--------- Slf4j WARN Log ---------");
		logger.debug("--------- Slf4j DEBUG  Log ---------");
		logger.info("--------- Slf4j iNFO Log ---------");
	}
	
	public static void printLog4j2Logs() {
		final MOILogger logger = new MOILog4j2LoggerImpl(MOILogFactory.class.getName());
		logger.trace("--------- Log4j TRACE  Log ---------");
		logger.error("--------- Log4j ERROR  Log ---------");
		logger.warn("--------- Log4j WARN Log ---------");
		logger.debug("--------- Log4j DEBUG  Log ---------");
		logger.info("--------- Log4j iNFO Log ---------");
	}
	
	public static void printJulLogs() {
		final Logger jul = Logger.getLogger("qa/gov/moi/logger/MOILogFactory.getName()");
		jul.info(   "--------- J.U.L. INFO    Log ---------");
		jul.warning("--------- J.U.L. WARNING Log ---------");
		jul.severe( "--------- J.U.L. SEVERE  Log ---------");
	}
}