package qa.gov.moi.util.resolver;

import java.util.List;
import java.util.Locale;

import javax.servlet.MultipartConfigElement;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartException;

import org.apache.commons.io.FileUtils;

/**
 * 
 * @ author Shahnawaz Alam
 *
 */

@ControllerAdvice
public class RestErrorHandler {

	private MessageSource messageSource;

	@Autowired
	MultipartConfigElement multipartConfigElement;
	
	@Autowired
	public RestErrorHandler(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@ExceptionHandler(AccessDeniedException.class)
	@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
	public @ResponseBody ResultDecorator handleAccessDeniedException(AccessDeniedException ex, HttpServletRequest request, HttpServletResponse response) throws Exception {

		ResultDecorator decorator = new ResultDecorator();
		decorator.setCode(ClientServerCodes.UNAUTHORIZED);
		if (ex.getCause() != null) {
			decorator.setMessage(ex.getCause().getMessage());
		} else {
			decorator.setMessage(ex.getMessage());
		}

		ex.printStackTrace();
		return decorator;

	}
	
	
	@ExceptionHandler(HttpMessageNotReadableException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResultDecorator handle(HttpMessageNotReadableException e) {
		ResultDecorator decorator = new ResultDecorator();
		decorator.setCode(400L);
		decorator.setMessage("Bad Request ,file size is more");
		return decorator;
	}
	
	
	@ExceptionHandler(MultipartException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public @ResponseBody ResultDecorator handleFileException(HttpServletRequest request, Exception ex) {
		ResultDecorator decorator = new ResultDecorator();
		decorator.setCode(413L);
		ex.printStackTrace();
		String maxSize=FileUtils.byteCountToDisplaySize(multipartConfigElement.getMaxFileSize());
		decorator.setMessage("File size excedeed. File size should be less than "+ maxSize);
		return decorator;
	}		
	
		
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public @ResponseBody ResultDecorator processValidationError(MethodArgumentNotValidException ex) {
		BindingResult result = ex.getBindingResult();
		List<FieldError> fieldErrors = result.getFieldErrors();

		return processFieldErrors(fieldErrors);
	}

	private ResultDecorator processFieldErrors(List<FieldError> fieldErrors) {
		ResultDecorator decorator = new ResultDecorator();
		if (fieldErrors != null && fieldErrors.size() > 0) {
			decorator = resolveResult(fieldErrors.get(0));
		}

		return decorator;

	}

	
	private ResultDecorator resolveResult(FieldError fieldError) {

		ResultDecorator rd = new ResultDecorator();
		String key = fieldError.getDefaultMessage();
		if (key != null)
			rd = keyBasedResolution(key);
		else
			rd = defaultResolutin(key);
		return rd;
	}

	public ResultDecorator keyBasedResolution(String key) {
		ResultDecorator rd = new ResultDecorator();
		//countries.put("Arabic", new Locale("ar", "DZ"));
		//or just language name for generic Arabic
		//new Locale("ar"); 
		String message = null;
		try {
			message = (messageSource.getMessage(key, null, Locale.ENGLISH));
		} catch (NoSuchMessageException e) {
			message = key;
		}
		rd.setMessage(message);
		rd.setCode(ValidationErrorCodesEnum.getErrorCode(key));
		return rd;
	}

	private ResultDecorator defaultResolutin(String key) {
		ResultDecorator rd = new ResultDecorator();

		return rd;
	}

	public RestErrorHandler() {

	}

	@ExceptionHandler(Exception.class)
	public @ResponseBody ResultDecorator handleGeneralException(Exception ex) {
		ResultDecorator decorator = new ResultDecorator();
		decorator.setCode(ClientServerCodes.FATEL_CODE);
		if (ex.getCause() != null) {
			decorator.setMessage(ex.getCause().getMessage());
		} else {
			decorator.setMessage(ex.getMessage());
		}
		
		if(decorator.getMessage()==null){
			decorator.setMessage("something went wrong, please contact Admin ASAP");
		}
		
		ex.printStackTrace();
		return decorator;
	}

}