package qa.gov.moi.util.loggerImpl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import qa.gov.moi.util.logger.MOILogger;
/**
 * 
 * @ author Shahnawaz Alam
 *
 */

public class MOILog4j2LoggerImpl implements MOILogger {
	private transient final Logger logger;

	public MOILog4j2LoggerImpl(String className) {
		System.setProperty("java.util.logging.manager", "org.apache.logging.log4j.jul.LogManager");
		logger = LogManager.getLogger(className);
	}

	public void trace(String message) {
		message = logger.getName() + ": " + message;
		logger.trace(message);
	}

	public void info(String message) {
		message = logger.getName() + ": " + message;
		logger.info(message);
	}

	public void debug(String message) {
		message = logger.getName() + ": " + message;
		logger.debug(message);
	}

	public void warn(String message) {
		message = logger.getName() + ": " + message;
		logger.warn(message);
	}

	public void error(String message) {
		message = logger.getName() + ": " + message;
		logger.error(message);
	}

	public void trace(String message, final Throwable thrower) {
		message = logger.getName() + ": " + message;
		logger.trace(message, thrower);
	}

	public void info(String message, final Throwable thrower) {
		message = logger.getName() + ": " + message;
		logger.info(message, thrower);
	}

	public void debug(String message, final Throwable thrower) {
		message = logger.getName() + ": " + message;
		logger.debug(message, thrower);
	}

	public void warn(String message, final Throwable thrower) {
		message = logger.getName() + ": " + message;
		logger.warn(message, thrower);
	}

	public void error(String message, final Throwable thrower) {
		message = logger.getName() + ": " + message;
		logger.error(message, thrower);
	}

}