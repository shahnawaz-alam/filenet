package qa.gov.moi.util.resolver;

/**
 * 
 * @ author Shahnawaz Alam
 *
 */

public enum ValidationErrorCodesEnum {
	GENERIC_WELCOME_MASSEGE(201L),
	RESET_SUCCESSFUL_SAVE(100L),
	UNSUCCESSFUL_AUTHENTICATE(200L),
	GENERIC_AUTH_SUCCESS(202L),
	TOKEN_EXPIRED(2001L),
	UNSUPPORTED_TOKEN(2002L),
	TOKEN_STRING_INVALID_JWS(2003L),
	JWS_SIGNATURE_VALIDATION_FAILS(2004L),
	TOKEN_STRING_INVALID(2005L),
	;
	
	private Long value;

	public Long getValue() {
		return value;
	}

	public void setValue(Long value) {
		this.value = value;
	}

	private ValidationErrorCodesEnum(Long value) {
		this.value = value;
	}

	public static Long getErrorCode(String key) {
		for (ValidationErrorCodesEnum code : ValidationErrorCodesEnum.values()) {
			if (code.toString().equals(key)) {
				return code.getValue();
			}
		}
		// throw an IllegalArgumentException or return null
		throw new IllegalArgumentException("the given number doesn't match any Status.");
	}

}
