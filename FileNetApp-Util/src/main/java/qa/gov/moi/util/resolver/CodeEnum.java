package qa.gov.moi.util.resolver;

/**
 * @author Shahnawaz Alam
 *
 */

public enum CodeEnum {
	GENERIC_WELCOME_MASSEGE("GENERIC_WELCOME_MASSEGE"),
	GEN_SUCCESSFUL("GEN_SUCCESSFUL"),
	GEN_FAILURE("GEN_FAILURE"),
	TOKEN_EXPIRED("TOKEN_EXPIRED"),
	UNSUPPORTED_TOKEN("UNSUPPORTED_TOKEN"),
	TOKEN_STRING_INVALID_JWS("TOKEN_STRING_INVALID_JWS"),
	JWS_SIGNATURE_VALIDATION_FAILS("JWS_SIGNATURE_VALIDATION_FAILS"),
	TOKEN_STRING_INVALID("TOKEN_STRING_INVALID"),

	
	RESET_SUCCESSFUL_SAVE("RESET_SUCCESSFUL_SAVE"),
	RESET_UNSUCCESSFUL_SAVE("RESET_UNSUCCESSFUL_SAVE"),
	CHANGE_SUCCESSFUL_SAVE("CHANGE_SUCCESSFUL_SAVE"),
	CHANGE_UNSUCCESSFUL_SAVE("CHANGE_UNSUCCESSFUL_SAVE"),	
	SUCCESSFUL_SAVE("GENERIC_REC_SAVE_SUCCS"),
	UNSUCCESSFUL_SAVE("GENERIC_REC_SAVE_CANT"),
	SUCCESSFUL_CANCEL("GENERIC_CANCEL_SUCCESSFUL"),
	UNSUCCESSFUL_CANCEL("GENERIC_CANCEL_UNSUCCESSFULT"),
	SUCCESSFUL_UPDATE("GENERIC_REC_UPDATE_SUCCS"),
	UNSUCCESSFUL_UPDATE("GENERIC_REC_UPDATE_CANT"),
	SUCCESSFUL_DELETE("GENERIC_DEL_SUCCS"),
	UNSUCCESSFUL_DELETE("GENERIC_DEL_CANT"),
	SUCCESSFUL_AUTHENTICATE("GENERIC_AUTH_SUCCESS"),
	UNSUCCESSFUL_AUTHENTICATE("GENERIC_AUTH_UNSUCCESSFUL"),
	
	GENERIC_TOT_REC_FOUND("GENERIC_TOT_REC_FOUND"),
	GENERIC_REC_FOUND("GENERIC_REC_FOUND"),
	GENERIC_NO_REC_FOUND("GENERIC_NO_REC_FOUND"),
	
	EXCEPTION_ON_REPORT_GENERATED("EXCEPTION_ON_REPORT_GENERATED");
	
	
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	private CodeEnum(String value) {
		this.value = value;
	}
		
	
}
