package qa.gov.moi.util.exception;
/**
 * @author Shahnawaz Alam
 *
 */
public class BusinessRuntimeException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	protected String code;
	
	public BusinessRuntimeException(String code) {
		super(code);
		this.code = code;
	}
	
	public BusinessRuntimeException() {
		super();
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
}