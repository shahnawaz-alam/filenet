package qa.gov.moi.util.exception;
/**
 * @author Shahnawaz Alam
 *
 */
public class GenericException extends BusinessException {
	private static final long serialVersionUID = 1L;

	public GenericException(String code, String message) {
		super(code, message);
	}

	public GenericException(Exception originalException, String rootCauseMessage) {
		super(originalException, rootCauseMessage);
	}

	public GenericException(String code, String message, Exception originalException, String rootCauseMessage) {
		super(code, message);
	}

}