package qa.gov.moi.util.mapper;

import ma.glasnost.orika.MapperFacade;

/**
 * 
 * @ author Shahnawaz Alam
 *
 */
public interface OrikaBeanMapper extends MapperFacade {

}
