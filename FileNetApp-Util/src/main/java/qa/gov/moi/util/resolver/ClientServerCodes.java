package qa.gov.moi.util.resolver;

/**
 * 
 * @ author Shahnawaz Alam
 *
 */

public interface ClientServerCodes {

	Long IDEAL_CODE = new Long(0);
	Long FATEL_CODE = new Long(1);
	Long FAILURE_CODE = new Long(2);
	Long UNAUTHORIZED_ACCESS_CODE=new Long(401);
	Long UNAUTHORIZED = new Long(100);
	Long VALUE_REQUIRED_CODE = new Long(3);
	Long IVALID_VALUE_CODE = new Long(4);
	Long NO_RECORD_CODE = new Long(5);

	

}
