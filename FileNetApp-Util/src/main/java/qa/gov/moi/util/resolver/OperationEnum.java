package qa.gov.moi.util.resolver;
/**
 * 
 * @ author Shahnawaz Alam
 *
 */

public enum OperationEnum 
{
	SAVE,
	UPDATE,
	DELETE,
	SEARCH,
	AUTHENTICATE,
	CANCEL,
	OTHER,
	COUNT,
	EXISTS;
	
}
