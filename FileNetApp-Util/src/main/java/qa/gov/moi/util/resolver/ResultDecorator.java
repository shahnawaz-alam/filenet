package qa.gov.moi.util.resolver;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * 
 * @ author Shahnawaz Alam
 *
 */

@JsonRootName(value="ResultDecorator")
public class ResultDecorator implements java.io.Serializable {

	private static final long serialVersionUID =1L;

	@JsonProperty(value="code")
	private Long code;
	@JsonProperty(value="message")
	private String message;
	@JsonProperty(value="result")
	private Object result;

	public ResultDecorator() {

	}

	public void finalize() throws Throwable {

	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	public Object getResult() {
		return result;
	}
	
	public void setResult(Object result) {
		this.result = result;
	}

	
	

}