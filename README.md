
# spring-security-with-jwt-implementation
# Architecture : Design & Developed By Team File-Net.

Reference : https://jwt.io/


WebSphere Application Server V8.5.5.0 service pack-18

Java --8.0,
Spring -- 5.3.0,
Spring Boot-- 2.6.4,
Maven -- 3.8.1,

Compitability
-------------

 For WebSphere Application Server Version 7.0 or later
 Spring -- 2.5.5 or later,
 Spring Boot -- 2.5.4 or later,
 Jdk -- 8/11/16 (LTS)
