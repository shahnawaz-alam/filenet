package qa.gov.moi.model.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 
 * @ author Shahnawaz Alam
 *
 */
@Entity
@Table(name="USERS")
public class User {
  
	@Id
	@SequenceGenerator(name = "userSequence", sequenceName = "user_seq",allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator ="userSequence")
	@Column(name="USER_ID")
    private int id;
    private String userName;
    private String password;
    private String email;
    
	public User() {
		super();
	}

	public User(int id, String userName, String password, String email) {
		super();
		this.id = id;
		this.userName = userName;
		this.password = password;
		this.email = email;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
    
}
