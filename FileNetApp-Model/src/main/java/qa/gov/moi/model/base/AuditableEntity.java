package qa.gov.moi.model.base;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

import qa.gov.moi.model.util.EntityConstants;

/**
 * 
 * @ author Shahnawaz Alam
 *
 */

@MappedSuperclass
@EntityListeners(value = { AuditEntityListener.class })
public class AuditableEntity {
	
	private Long createdBy;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = EntityConstants.DATE_TIME_PATTERAN_FULL)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;
	
	private Long lastModifiedBy;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = EntityConstants.DATE_TIME_PATTERAN_FULL)
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModifiedDate;
	
	@Column(name = "created_by", insertable=true, updatable=false)
	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}


	@Column(name = "created_on", insertable=true, updatable=false)
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "updated_by", insertable=false, updatable=true)
	public Long getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(Long lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	@Column(name = "updated_on", insertable=false, updatable=true)
	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

}