/**
 * 
 */
package qa.gov.moi.model.util;

/**
 * 
 * @ author Shahnawaz Alam
 *
 */
public interface EntityConstants {
	
	String SCHEMA = "devdb"; 
	String DATE_TIME_PATTERAN_yyyMMdd = "yyyy-MM-dd";
	String DATE_TIME_PATTERAN_FULL = "dd-MM-yyyy hh:mm:ss a";

}
