package qa.gov.moi.model.base;

import java.util.Date;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

/**
 * 
 * @ author Shahnawaz Alam
 *
 */

public class AuditEntityListener {

	@PrePersist
	public void prePersist(AuditableEntity e) {
		 e.setCreatedBy(1L);
		 e.setCreatedDate((new Date()));
		
//		  UserMast um = SecurityUtil.getLoggedUser(); 
//		  if(um != null){
//			  	e.setCreatedBy(um.getUserId()); 
//		  		} 
//		  e.setCreatedDate((new Date()));
	}

	@PreUpdate
	public void preUpdate(AuditableEntity e) {
		
		  e.setLastModifiedBy(2L);
		  e.setLastModifiedDate((new Date()));
		 
		
		/*
		 * UserMast um = SecurityUtil.getLoggedUser(); if (um != null) {
		 * e.setLastModifiedBy(um.getUserId()); } e.setLastModifiedDate((new Date()));
		 */
		 
	}
}