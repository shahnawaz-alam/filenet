package qa.gov.moi.dao.repo;


import org.springframework.data.jpa.repository.JpaRepository;

import qa.gov.moi.model.entity.user.User;

/**
 *
 * @ author Shahnawaz Alam
 *
 */

public interface UserRepository extends JpaRepository<User,Integer> {
    public User findByUserName(String username);
}
