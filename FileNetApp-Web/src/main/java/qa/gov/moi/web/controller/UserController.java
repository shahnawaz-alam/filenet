/**
 * 
 */
package qa.gov.moi.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import qa.gov.moi.service.UserService;
import qa.gov.moi.util.exception.BusinessException;
import qa.gov.moi.util.logger.MOILogger;
import qa.gov.moi.util.loggerImpl.MOILogFactory;
import qa.gov.moi.util.resolver.OperationEnum;
import qa.gov.moi.util.resolver.RestErrorHandler;
import qa.gov.moi.util.resolver.ResultDecorator;
import qa.gov.moi.util.resolver.ResultDecoratorHandler;

/**
 * 
 * @ author Shahnawaz Alam
 *
 */


@RestController
@RequestMapping(value = "/login")
public class UserController {
	
	private static final MOILogger logger = MOILogFactory.getLoggerInstance(UserController.class.getName());
	
	@Autowired
	UserService userService;

	@Autowired
	private ResultDecoratorHandler handler;

	@Autowired
	private RestErrorHandler restErrorHandler;
	
	
	@RequestMapping(value = "getUser", method = RequestMethod.GET, produces = { "application/json","application/xml" })
	public @ResponseBody ResultDecorator getUser() throws BusinessException {
		logger.info("---------------All UserDetails--------------------");
		String msg = "Welcome To Ministry of Interior";
		return handler.resolveResult(msg,OperationEnum.OTHER);
	}

}
