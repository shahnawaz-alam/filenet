package qa.gov.moi.web.security;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import qa.gov.moi.service.serviceImpl.CustomUserDetailsService;
import qa.gov.moi.util.exception.BusinessException;
import qa.gov.moi.util.resolver.CodeEnum;
import qa.gov.moi.util.security.JwtTokenUtil;

/**
 * 
 * @ author Shahnawaz Alam
 *
 */

@Component
public class JwtFilter extends OncePerRequestFilter {

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private JwtTokenUtil jwtUtil;

	@Autowired
	private CustomUserDetailsService service;
	
	@Value("${jwt.header}")
    private String header;

	@Override
	protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			FilterChain filterChain) throws ServletException, IOException {
		String token = null;
		String userName = null;
		String authorizationHeader = httpServletRequest.getHeader(header);

		if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
			token = authorizationHeader.substring(7);
			
				try {
					userName = getUserName(token);
				} catch (BusinessException e) {
					logger.error(e);
				}	

		}

		if (userName != null && SecurityContextHolder.getContext().getAuthentication() == null) {
			UserDetails userDetails = service.loadUserByUsername(userName);

			if (jwtUtil.validateToken(token, userDetails)) {

				UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
						userDetails, null, userDetails.getAuthorities());
				usernamePasswordAuthenticationToken
						.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
				SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
			}
		}
		filterChain.doFilter(httpServletRequest, httpServletResponse);
	}
	
	private String getUserName(String token) throws BusinessException {
		String userName = null;
		try {
			userName = jwtUtil.extractUsername(token);
		} catch (ExpiredJwtException ee) {
			logger.error("Specified JWT is a Claims JWT and the Claims has an expiration time");
			throw new BusinessException("TOKEN_EXPIRED",
					messageSource.getMessage(CodeEnum.TOKEN_EXPIRED.getValue(), null, Locale.ENGLISH));
		} catch (UnsupportedJwtException us) {
			logger.error("Argument does not represent an Claims JWS");
			throw new BusinessException("UNSUPPORTED_TOKEN",
					messageSource.getMessage(CodeEnum.UNSUPPORTED_TOKEN.getValue(), null, Locale.ENGLISH));
		} catch (MalformedJwtException me) {
			logger.error("String is not a valid JWS");
			throw new BusinessException("TOKEN_STRING_INVALID_JWS",
					messageSource.getMessage(CodeEnum.TOKEN_STRING_INVALID_JWS.getValue(), null, Locale.ENGLISH));
		} catch (SignatureException se) {
			logger.error("JWS signature validation fails");
			throw new BusinessException("JWS_SIGNATURE_VALIDATION_FAILS",
					messageSource.getMessage(CodeEnum.JWS_SIGNATURE_VALIDATION_FAILS.getValue(), null, Locale.ENGLISH));
		} catch (IllegalArgumentException ae) {
			logger.error("JWS signature validation fails");
			throw new BusinessException("TOKEN_STRING_INVALID",
					messageSource.getMessage(CodeEnum.TOKEN_STRING_INVALID.getValue(), null, Locale.ENGLISH));
		}

		return userName;
	}
}
