package qa.gov.moi.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author Shahnawaz Alam
 *
 */

 @SpringBootApplication (exclude = { SecurityAutoConfiguration.class })// This disabled the Spring Basic Security
//@SpringBootApplication
@EnableJpaRepositories(basePackages = "qa.gov.moi.dao.repo", repositoryImplementationPostfix = "CustomImpl")
@ComponentScan(basePackages = { "qa.gov.moi.web","qa.gov.moi.util","qa.gov.moi.service" })
@EntityScan(basePackages = "qa.gov.moi.model.entity")
public class Application extends ServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}