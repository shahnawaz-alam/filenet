package qa.gov.moi.web.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import qa.gov.moi.service.UserService;
import qa.gov.moi.service.dto.AuthRequestDto;
import qa.gov.moi.util.exception.BusinessException;
import qa.gov.moi.util.logger.MOILogger;
import qa.gov.moi.util.loggerImpl.MOILogFactory;
import qa.gov.moi.util.resolver.OperationEnum;
import qa.gov.moi.util.resolver.RestErrorHandler;
import qa.gov.moi.util.resolver.ResultDecorator;
import qa.gov.moi.util.resolver.ResultDecoratorHandler;
import qa.gov.moi.web.utils.ControllerUtils;

/**
 * 
 * @ author Shahnawaz Alam
 *
 */

@RestController
public class AuthenticationController {

	private static final MOILogger logger = MOILogFactory.getLoggerInstance(AuthenticationController.class.getName());

	@Autowired
	UserService userService;

	@Autowired
	private ResultDecoratorHandler handler;

	@Autowired
	private RestErrorHandler restErrorHandler;


	@RequestMapping(value = ControllerUtils.AUTHENTICATE, method = RequestMethod.POST, produces = { "application/json","application/xml" })
	public @ResponseBody ResultDecorator generateToken( @Valid @RequestBody  AuthRequestDto authRequest, Errors errors) throws Exception {
		logger.info("---------------Authenticate User--------------------");
		if (errors.hasErrors()) {
			throw new BusinessException(BusinessException.REQ_ERROR, errors.getFieldError().getDefaultMessage());
		}
		try {
			return handler.resolveResult(userService.authenticateUser(authRequest), OperationEnum.AUTHENTICATE);

		} catch (BusinessException e) {
			logger.error("Error Code : " + e.getCode() + " Error Message : ", e);
			ResultDecorator errorDecorator = restErrorHandler.keyBasedResolution(e.getCode());
			return errorDecorator;
		} catch (Exception e) {
			logger.error("Exception Occured in UserController:" + " ", e);
			ResultDecorator errorDecorator = restErrorHandler.keyBasedResolution("ERROR_MSG_UNKNOWN_EXCEPTION");
			return errorDecorator;
		}
	}
	
}
